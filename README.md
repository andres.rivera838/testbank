# Bank

# Inicio Servidor node (api request) 
* Se debe utilizar una version de node > 8
* La base de datos se encuentra alojada en los servidores de Mlab se utilizo mongooDB
* Instalar dependencias (npm) 
* Abrimos la consola en la carpeta back-end
```
    npm install
```
* El servidor se inciara en el puerto localhost:3000, por defecto sera la variable de entorno (PORT) si existe;

**Develop**
```
    npm run start-dev 
```

**Production**
``` 
    npm run start
```

**Api**

* Agregar cliente
    ``` 
        GET localhost:3000/api/add/customer
    ```
* Agregar credito   
    ``` 
        GET localhost:3000/api/add/credit
    ```
* Obtener clientes
    ``` 
        GET localhost:3000/api/get/customer
    ```
* Obtener creditos
    ``` 
        GET localhost:3000/api/get/credit
    ```


# Iniciar page 

* la pagina se realizo en **Angular6**

* Instalar dependencias (npm)
* abrimos la consola en la carpeta front-end/bank
```
    npm install
```

**develop**
```
ng serve --o
```
**production**
```
ng build --prod
```

    

