'use strict'

const debug = require('debug')('testbank:api:routes')
const express = require('express')
const asyncify = require('express-asyncify')

const config = require('../config')

const api = asyncify(express.Router())

const bodyParser = require('body-parser');

api.use(bodyParser.urlencoded({'extended': true}))
api.use(bodyParser.json({ type: '*/*' }))

const customer = require('../schema/schemaCustomer')
const credit = require('../schema/schemaCredit')


let services;

api.use('*', async (req, res, next) => {
  if (!services) {
    debug('Connecting to services')
    try {
      services = await config.db
    } catch (e) {
      return next(e)
    }
  }
  next()
})

api.post('/add/customer', async (req, res, next) => {
  console.log('TCL: res', res);
  console.log('TCL: customer')
  const { name, lastName, id, dateBorn} = req.body

  let NewCustomer = new customer({name, lastName, id, dateBorn})

    customer.collection.insert([NewCustomer])
    .then((data)=>{
      console.log('TCL: data', data);
      return response(res, data.ops, 200);
    }).catch((err)=>{
      console.log('TCL: err', err);
      response(res, err, 500 )
    });
});

api.post('/add/credit', async (req, res, next) => {
  console.log('TCL: Credit');
  const { companyName, nit, salary, date, active} = req.body

  let NewCredit = new credit({companyName, nit, salary, date, active})

  credit.collection.insert([NewCredit])
  .then((data)=>{
    response(res, data.ops, 200)
  }).catch((err)=>{
    response(res, err, 500)
  })
});

api.get('/get/credit', async (req, res, next) => {
  console.log('TCL: get Credit');

  credit.find()
  .then(async (doc)=>{
      response(res, doc, 200)  
  })
  .catch((err)=>{
    response(res, err, 500)
  });
});

api.get('/get/customer', async (req, res, next) => {
  console.log('TCL: get customer');

  customer.find()
  .then(async (doc)=>{
      response(res, doc, 200)
  })
  .catch((err)=>{
    response(res, err, 500)
  });
});

function response(res, data, status){
  return res.status(status).send({status:status, data: data})
}

module.exports = api
