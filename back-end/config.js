'use strict'

const debug = require('debug')('testBank:api:db')
const client = require('mongoose');

const urlMongo = "mongodb://testBank:testBank1@ds157843.mlab.com:57843/banco"


var db = client.connect(urlMongo, (err, success)=>{

  if(err){
    debug(`Error: ${err.message}`)
  }else{
    debug('Connecting to dataBase')
    
    return success
  }
})

module.exports = {
  db
}
