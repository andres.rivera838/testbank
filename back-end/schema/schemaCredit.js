const mongoose = require('mongoose');

const creditSchema = mongoose.Schema({
    companyName: String,
    nit: String,
    salary: Number,
    date: String,
    value: Number,
    state: Boolean
});
module.exports = mongoose.model('credit',creditSchema);