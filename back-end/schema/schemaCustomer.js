const mongoose = require('mongoose');

const customerSchema = mongoose.Schema({
    name: String,
    lastName:String,
    id: Number,
    dateBorn: String
});
module.exports = mongoose.model('customer',customerSchema);