'use strict'

const test = require('ava')
const util = require('util')
const request = require('supertest')
const sinon = require('sinon')
const api = require('../api/api.js')


test('success add customer', async t => {
  const response = await request(api)
    .post('/add/customer')
    .send({
      "name": "a",
      "lastName": "s",
      "id": "1234",
      "dateBorn": "23"
    });

    t.is(response.status, 200);
    console.log('TCL: response', response);
})