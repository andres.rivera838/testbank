'use strict'

const debug = require('debug')('testBank:api')

function createMsg(status, err){
    return {
        status,
        data: err
    }
}

module.exports = {
    error: (res, status, err)=>{
        console.log('TCL: res, status, err', res, status, err);
        res.sendStatus(status);
        res.send(createMsg(status, err))
    }
}