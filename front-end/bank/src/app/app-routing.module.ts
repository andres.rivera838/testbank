import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CustomerComponent } from './customer/customer.component';
import { CreditComponent } from './credit/credit.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {path: 'customer', component: CustomerComponent},
  {path: 'credit', component: CreditComponent},
  {path: 'home', component: HomeComponent},
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
