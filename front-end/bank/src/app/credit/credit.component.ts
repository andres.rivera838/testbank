import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ApiService } from '../service/api.service';
import { NgbDatepickerConfig, NgbCalendar, NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.scss']
})
export class CreditComponent implements OnInit {

  public formCredit: FormGroup;
  public today: any = new Date();
  public dateNoValid: Boolean;
 
  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    config: NgbDatepickerConfig, 
    calendar: NgbCalendar,
  ) {

    // customize default values of datepickers used by this component tree
    config.minDate = {year: 1900, month: 1, day: 1};
    config.maxDate = {year: this.today.getFullYear() , month: this.today.getMonth() + 1, day: this.today.getDate()};
    // days that don't belong to current month are not visible
    config.outsideDays = 'hidden';

    // weekends are disabled

    this.formCredit = this.formBuilder.group({
      companyName: ['',
        [Validators.required]
      ],
      nit: ['',
        [Validators.required, Validators.pattern('^\\d{10}$'), Validators.minLength(9), Validators.maxLength(10)]
      ],
      nitVerifi: ['',
        [Validators.required, Validators.pattern('^\\d{1}$'), Validators.maxLength(1)]
      ],
      salary: ['',
        [Validators.required]
      ],
      date: ['',
        [Validators.required]
      ]
    });
  }

  ngOnInit() {
  }


  onSubmitCredit() {

    let date: any = new Date(this.formCredit.value.date.year + '/' + this.formCredit.value.date.month + '/' + this.formCredit.value.date.day);

    var diff = this.today - date;
    var dias = Math.floor(diff / (1000 * 60 * 60 * 24));

    if(dias > 547){

      let value;
      let nit = this.formCredit.value.nit + "-" + this.formCredit.value.nitVerifi;
      if(this.formCredit.value.salary >= 800000 && this.formCredit.value.salary <= 1000000){
        value = 5000000;
      }else if(this.formCredit.value.salary > 1000000 && this.formCredit.value.salary <= 4000000){
        value = 20000000;
      }else if(this.formCredit.value.salary > 4000000 ){
        value = 50000000;
      }


      let param = {       
        companyName: this.formCredit.value.companyName,
        nit: nit,
        salary: this.formCredit.value.salary,
        date: date,
        value: value
      }

      console.log('TCL: CreditComponent -> onSubmitCredit -> param', param);

      this.apiService.eventPost('add/credit', param).subscribe((data) => {
        console.log('TCL: CustomerComponent -> onSubmitCustomer -> data', data);
        if (!data.error) {
          alert("credito agregado correctamente");
          this.formCredit.reset();
        }
      })
    }else{
      this.dateNoValid = true;
    }

  }

}
