import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NgbDatepickerConfig, NgbCalendar, NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from '../service/api.service'
@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
  providers: [NgbDatepickerConfig] 
})
export class CustomerComponent implements OnInit {

  public formCustomer: FormGroup;
  public model: NgbDateStruct;
  public today: any = new Date();


  constructor(
    private formBuilder: FormBuilder,
    config: NgbDatepickerConfig, 
    calendar: NgbCalendar,
    private apiService: ApiService
  ) {


      // customize default values of datepickers used by this component tree
      config.minDate = {year: 1900, month: 1, day: 1};
      config.maxDate = {year: this.today.getFullYear() , month: this.today.getMonth() + 1, day: this.today.getDate()};
      // days that don't belong to current month are not visible
      config.outsideDays = 'hidden';
  
      // weekends are disabled

    this.formCustomer = this.formBuilder.group({
      name: [
        '',
        [Validators.required]
      ],
      lastName: [
        '',
        [Validators.required]
      ],
      id: [
        '',
        [Validators.required, Validators.pattern('^\\d{10}$')]
      ],
      dateBorn: [
        '',
        [Validators.required]
      ]
    });
  }

  ngOnInit() {
  }

  onSubmitCustomer(){

    let dateBorn: any = new Date(this.formCustomer.value.dateBorn.year + '/' + this.formCustomer.value.dateBorn.month + '/' + this.formCustomer.value.dateBorn.day);
    var diff = this.today.getFullYear() - this.formCustomer.value.dateBorn.year;

    this.formCustomer.value.dateBorn = dateBorn;

    if(diff >= 18){
      this.apiService.eventPost('add/customer', this.formCustomer.value).subscribe((data)=>{
        console.log('TCL: CustomerComponent -> onSubmitCustomer -> data', data);
        if(!data.error){
          alert("Cliente agregado correctamente");
          this.formCustomer.reset();
        }
      })
    }    
  }

}
