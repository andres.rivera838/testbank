import { Component, OnInit } from '@angular/core';
import { ApiService } from '../service/api.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public customer;
  public credit;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getCustomer();
    this.getCredit();
  }

  getCustomer() {
    this.apiService.eventGet('get/customer').subscribe((data) => {
      console.log('TCL: HomeComponent -> ngOnInit -> data', data);
      this.customer = data;
    })
  }

  getCredit(){
    this.apiService.eventGet('get/credit').subscribe((data) => {
      console.log('TCL: HomeComponent -> ngOnInit -> data', data);
      this.credit = data;
    })
  }
}
