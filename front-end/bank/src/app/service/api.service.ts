import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { from, throwError, of } from 'rxjs';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private server:string = 'http://localhost:3000/api'

  constructor(private httpClient:HttpClient) { }

  eventPost(urlService, data){    
    return this.httpClient.post(`${this.server}/${urlService}`, data)
        .pipe(
          map(data =>  {return this.validateData(data)}),
          catchError(this.handleError)
        )
  }

  eventGet(urlService){    
    return this.httpClient.get(`${this.server}/${urlService}`)
        .pipe(
          map(data =>  {return this.validateData(data)}),
          catchError(this.handleError)
        )
  }

  validateData(data){
    console.log('TCL: ApiService -> validateData -> data', data);
    if(data.status == 200){
      return data.data;
    }else{
      alert("Ocurrio un error");
      console.error(data);
      return {error: true, data}
    }
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  };
}
